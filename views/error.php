<?php
if(isset($this->coreui)) {
    require 'template_head_start.php';
    require 'template_head_end.php';
}
?>

<!-- Error Content -->
<div class="content bg-white text-center pulldown overflow-hidden">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <!-- Error Titles -->
            <?php switch($code) {
                case 500:
            ?>
            <h1 class="font-s128 font-w300 text-primary animated zoomInDown"><?php echo $code; ?></h1>
            <h2 class="h3 font-w300 push-50 animated fadeInUp">We are sorry but our server encountered an internal error...</h2>
            <?php
                break;
                case 404 :
            ?>
            <h1 class="font-s128 font-w300 text-primary animated zoomInDown"><?php echo $code; ?></h1>
            <h2 class="h3 font-w300 push-50 animated fadeInUp">We are sorry but the page you are looking for was not found...</h2>
            <?php
                break;
            } ?>
            <!-- END Error Titles -->
            <?php if($this->config->config('environment') != 'production') : ?>
            <button type="button" style="margin:2em;" class="btn btn-sm btn-danger" onclick="App.blocks('#error-detail', 'content_toggle');">
                Show Error Detail
            </button>

            <div class="block block-rounded block-opt-hidden" style="margin-top: 1em; margin-bottom: 2em;" id="error-detail">
                <div class="block-content block block-bordered">
                    <?php echo $errmsg; ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<!-- END Error Content -->

<!-- Error Footer -->
<div class="content pulldown text-muted text-center">
    Would you like to let us know about it?<br>
    <a class="link-effect" href="<?php echo $this->location('report','core'); ?>">Report it</a>
    or <a class="link-effect" href="<?php echo $this->location('dashboard','core'); ?>">Go Back to Dashboard</a>
</div>
<!-- END Error Footer -->

<?php
if(isset($coreui)) {
    require 'template_footer_start.php';
    require 'template_footer_end.php';
}