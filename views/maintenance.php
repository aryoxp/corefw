<?php
if(isset($this->coreui)) {
    require 'template_head_start.php';
    require 'template_head_end.php';
}
?>

<!-- Error Content -->
<div class="content bg-white text-center pulldown overflow-hidden">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">

            <h1 class="font-s64 font-w300 text-primary animated zoomInDown">Server Maintenance</h1>
            <h2 class="h3 font-w300 push-50 push-15-t animated fadeInUp">We are sorry but our server is currently under maintenance.<br>Please try again later.</h2>

        </div>
    </div>
</div>
<!-- END Error Content -->

<?php
if(isset($coreui)) {
    require 'template_footer_start.php';
    require 'template_footer_end.php';
}