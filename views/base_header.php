<?php
/**
 * base_header.php
 *
 * Author: pixelcave
 *
 * The header of each page (Backend)
 *
 */
?>

<!-- Header -->
<header id="header-navbar" class="content-mini content-mini-full">
    <!-- Header Navigation Right -->
    <ul class="nav-header pull-right">
        <li>
            <div class="btn-group">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button">

                    <img src="<?php echo $this->coreui->assets_folder; ?>/img/avatars/<?php echo (isset($_COOKIE['aid'])?$_COOKIE['aid']:'12'); ?>.png" alt="Avatar"
                         style="width: 18px;" class="push-5-r" >

                    <span class="hidden-sm hidden-xs"><?php echo $this->core->get_user_name(); ?></span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <div href="#" style="padding: 7px 12px">
                            <?php if($status = trim($this->core->get_user()->status)) { ?>
                            <em class="text-primary">"<?php echo $this->core->get_user()->status; ?>"</em>
                            <?php } else { ?>
                            <em class="text-muted">"No status"</em>
                            <?php } ?>
                        </div>
                    </li>
                    <li class="divider"></li>
                    <li class="dropdown-header">Profile</li>
                    <li>
                        <a tabindex="-1" href="<?php echo $this->location('message', $this->core->config('default_app')); ?>">
                            <i class="si si-envelope-open pull-right"></i>
                            <span class="badge badge-success pull-right">3</span>
                            Messages
                        </a>
                    </li>
                    <li>
                        <a tabindex="-1" href="<?php echo $this->location('user/profile', $this->core->config('default_app')); ?>">
                            <i class="si si-user pull-right"></i>
                            <!--<span class="badge badge-success pull-right">1</span>-->
                            Profile
                        </a>
                    </li>
                    <li>
                        <a tabindex="-1" href="<?php echo $this->location('user/password', $this->core->config('default_app')); ?>">
                            <i class="si si-key pull-right"></i>Change password
                        </a>
                    </li>
                </ul>
                <button class="btn btn-danger btn-logoff"
                        href="<?php echo $this->location('auth/logoff', $this->core->config('default_app')); ?>"
                        type="button">
                    <i class="si si-logout push-5-r"></i> <span class="hidden-sm hidden-md hidden-xs">Log out</span>
                </button>
            </div>
        </li>
        <li>
            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
            <button class="btn btn-default" data-toggle="layout" data-action="side_overlay_toggle" type="button">
                <i class="fa fa-tasks"></i>
            </button>
        </li>
    </ul>
    <!-- END Header Navigation Right -->

    <!-- Header Navigation Left -->
    <ul class="nav-header pull-left">
        <li class="hidden-md hidden-lg">
            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
            <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
                <i class="fa fa-navicon"></i>
            </button>
        </li>
        <li class="hidden-xs hidden-sm">
            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
            <button class="btn btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
                <i class="fa fa-ellipsis-v"></i>
            </button>
        </li>
        <li class="visible-xs">
            <!-- Toggle class helper (for .js-header-search below), functionality initialized in App() -> uiToggleClass() -->
            <button class="btn btn-default" data-toggle="class-toggle" data-target=".js-header-search" data-class="header-search-xs-visible" type="button">
                <i class="fa fa-search"></i>
            </button>
        </li>
        <li class="js-header-search header-search">
            <?php $controller = str_replace("controller_", '', $this->core->controller); ?>
            <form class="form-horizontal" action="<?php echo $this->location($controller.'/search'); ?>" method="post">
                <div class="form-material form-material-primary input-group remove-margin-t remove-margin-b">
                    <input class="form-control" type="text" id="base-material-text" name="base-material-text" placeholder="Search...">
                    <span class="input-group-addon"><i class="si si-magnifier"></i></span>
                </div>
            </form>
        </li>
        <?php if($coremenu): ?>
        <li class="hidden-xs hidden-sm">
            <div class="btn-group-sm">
                <?php foreach($coremenu as $m) { //var_dump($m);
                    echo '<a href="'.$m->url.'" class="btn btn-'.$m->type.'">'.$m->label.'</a>';
                } ?>
            </div>

        </li>
        <?php endif; ?>
    </ul>
    <!-- END Header Navigation Left -->
</header>
<!-- END Header -->
