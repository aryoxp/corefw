<!-- Error Content -->
<div class="content bg-white text-center pulldown overflow-hidden">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <!-- Error Titles -->
            <h1 class="font-s128 font-w300 text-primary animated zoomInDown">403</h1>
            <h2 class="h3 font-w300 push-50 animated fadeInUp">Sorry, you are not authorized to access this page.</h2>
            <!-- END Error Titles -->
        </div>
    </div>
</div>
<!-- END Error Content -->

<!-- Error Footer -->
<div class="content pulldown text-muted text-center">
    Would you like to let us know about it?<br>
    <a class="link-effect" href="<?php echo $this->location('report','core'); ?>">Report it</a>
    or <a class="link-effect" href="<?php echo $this->location('dashboard','core'); ?>">Go Back to Dashboard</a>
</div>
<!-- END Error Footer -->