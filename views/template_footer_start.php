<?php
/**
 * template_footer_start.php
 *
 * Author: pixelcave
 *
 * All vital JS scripts are included here
 *
 */
?>

        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <script src="<?php echo $coreui->assets_folder; ?>/js/core/jquery.min.js"></script>
        <script src="<?php echo $coreui->assets_folder; ?>/js/core/bootstrap.min.js"></script>
        <script src="<?php echo $coreui->assets_folder; ?>/js/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
        <script src="<?php echo $coreui->assets_folder; ?>/js/core/jquery.slimscroll.min.js"></script>
        <script src="<?php echo $coreui->assets_folder; ?>/js/core/jquery.scrollLock.min.js"></script>
        <script src="<?php echo $coreui->assets_folder; ?>/js/core/jquery.appear.min.js"></script>
        <script src="<?php echo $coreui->assets_folder; ?>/js/core/jquery.countTo.min.js"></script>
        <script src="<?php echo $coreui->assets_folder; ?>/js/core/jquery.placeholder.min.js"></script>
        <script src="<?php echo $coreui->assets_folder; ?>/js/core/js.cookie.min.js"></script>
        <?php
echo '<script src="'
    . $this->location('lib/js/'.$this->coreapp."/".$this->core->controller."/".$this->core->method, $coredefaultapp)
    . '"></script>' . "\n";
?>
        <script src="<?php echo $coreui->assets_folder; ?>/js/app.js"></script>
        <script src="<?php echo $coreui->assets_folder; ?>/js/coreui.js"></script>

        <!-- Page Plugins and JS Code -->
        <?php /*
        <script src="<?php echo $coreui->assets_folder; ?>/js/plugins/slick/slick.min.js"></script>
        <script src="<?php echo $coreui->assets_folder; ?>/js/plugins/chartjs/Chart.min.js"></script>
        */ ?>

<?php

    if(isset($this->corescripts)) {
        foreach ($this->corescripts as $s) {
            echo "\t".'<script src="' . $s . '"></script>' . "\n";
        }
    }

    if(isset($corenotifications) and is_array($corenotifications) and count($corenotifications)) {
        echo '<script type="text/javascript">';
        echo '   $(function(){';
        foreach($corenotifications as $n) {
            $sticky = 'false';
            if($n[2] == true)
                $sticky = 'true';

            switch($n[1]) {
            case libnotification::error:
                $icon = 'fa fa-times';
                $type = 'danger';
                break;
            case libnotification::success:
                $icon = 'fa fa-check';
                $type = 'success';
                break;
            case libnotification::warning:
                $icon = 'fa fa-warning';
                $type = 'warning';
                break;
            default:
                $icon = 'fa fa-info-circle';
                $type = 'info';
                break;
            }
            ?>showAlert('<?php echo addslashes($n[0]); ?>','<?php echo $type; ?>', <?php echo $sticky; ?>);<?php
        }
        echo '    });';
        echo '</script>';
    }
