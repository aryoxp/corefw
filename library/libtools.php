<?php

class libtools {

	public static function arrayToObject($array) {
		if(!is_array($array)) {
			return $array;
		}
		
		$object = (object) null;
		if (is_array($array) && count($array) > 0) {
		  foreach ($array as $name=>$value) {
			 $name = strtolower(trim($name));
			 if (!empty($name)) {
				$object->$name = self::arrayToObject($value);
			 }
		  }
		  return $object;
		}
		else {
		  return FALSE;
		}
	}

    /**
     * Get Base URL
     *
     * @author  Aris S Ripandi
     * @since	Version 0.3.1
     *
     * @access public
     * @return void
     */
    public static function base_url(){
        $base_url  = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';	// protocol
        $base_url .= preg_replace('/:(80|443)$/', '', $_SERVER['HTTP_HOST']);							// host[:port]
        $base_url .= str_replace('\\', '/', dirname($_SERVER['SCRIPT_NAME']));							// path
        if (substr($base_url, -1) == '/') $base_url = substr($base_url, 0, -1);
        $base_url = $base_url . '/';
        return $base_url;
    }

}
