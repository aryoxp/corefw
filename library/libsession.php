<?php defined('CORE') or die('No direct access!');

class libsession implements interface_session {
    
    private $core;
    private $driver;
    
    public function __construct( $core ) {

		$this->core = $core;
		$session_config = $this->core->config('session');
        require_once CORE . 'library/session/' . $session_config->driver.'.php';
        $class_name = 'session_'.$session_config->driver;
        $this->driver = new $class_name( $session_config );
				
    }
	
	// session variables getter and setter 
	public function set( $name, $value = NULL ) {
		$this->driver->set( $name, $value );
	}
	public function get( $name ) {
		return $this->driver->get( $name );
	}
	
	// remove/unset session variable(s)
	public function remove( $name ) {
		$this->driver->remove( $name );
	}
	
	// completely destroy session
	public function destroy() {
		$this->driver->destroy();
	}
	
	// set no session
	public function nocache() {
		$this->driver->nocache();
	}
	
	// get the session id
	public function id() {
		$this->driver->id();
	}

}
