<?php defined('CORE') or die("This file can't be accessed directly!");

class libcore {

    private $autoload;

    public $error;
    public $config;

    public $app;
    public $controller;
    public $method;

    private $user;


    public function __construct(){

        define('STARTTIME', microtime(true));

        ini_set ('magic_quotes_gpc', 0);

        // defining system and application structure paths)
        define('LIBRARY', CORE. 'library/');
        define('CONTROLLER', APPLICATION . 'controller/');
        define('MODEL', APPLICATION . 'model/');
        define('VIEW', APPLICATION . 'view/');
        define('APPLIBRARY', APPLICATION . 'library/');
        define('APPBASE', dirname(APPLICATION));

        require_once LIBRARY . 'liberror.php';
        require_once LIBRARY . 'libautoloader.php';

        $this->autoload = new libautoloader();
        $this->config = new libconfig();
        $this->config->load();
        define('CORELIBRARY', APPBASE . "/" . $this->config('default_app') . "/library/");
        define('CACHE', APPLICATION . $this->config('cache_folder') . "/");

        $this->error = new liberror( $this->config );
        $this->autoload->initcorelib();

        if($this->config->config('default_timezone'))
            date_default_timezone_set($this->config->config('default_timezone'));
        else date_default_timezone_set('Asia/Jakarta');

    }

    public function start() {

        if($this->config('maintenance')) {
            $libmaintenance = new libmaintenance( $this->config );
            $libmaintenance->show();
            exit;
        }

        $uri = new liburi( $this->config->get_config() );
        $this->controller = $uri->getController();  //var_dump($this->controller);
        $this->method = $uri->getMethod();          //var_dump($this->method);

        try {

            if ( !$this->controller ) { // invalid controller
                $this->error->add( "Invalid controller or no controller could be found." );
                $this->error->show();
                exit;
            }

            $this->initiate_user();

            $P = new $this->controller( $this ); // instantiate the controller

            if( method_exists( $this->controller, $this->method ) ) {
                call_user_func_array( array($P, $this->method), $uri->getArgs() ); // execute method
            } else {
                $this->error->add( "Method <code>" . $this->method . "</code> not exists in <code>" . $this->controller . "</code> controller " );
                $this->error->show();
                exit;
            }

        } catch (Exception $e) {
            // catch errors in case controller fails to load
            //$this->error = new error( $e->getMessage() );
            $this->error->add( $e->getMessage() );
            $this->error->show();
            exit;

        }

    }

    public function getExecutionTime() {
        return microtime( true ) - $GLOBALS['microtime_start'];
    }

    public function cleanup() {
        // do clean up here...
    }

    public function config($key) {
        return $this->config->config($key);
    }

    public function is_development() {
        return ($this->config('environment') != 'production');
    }

    private function initiate_user() {
        $sess = new libsession( $this );
        $this->user = $sess->get('user'); //var_dump($this->user);
    }

    public function get_user() {
        return $this->user;
    }

    public function get_user_name() {
        return ($this->user?$this->user->name:'Anonymous');
    }

    public function autoload_appmodel($app) {
        $this->autoload->autoload_appmodel($app);
    }


}