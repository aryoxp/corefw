<?php defined('CORE') or die("This file can't be accessed directly!");

class controller extends basecontroller {

    private $core;
    private $corebare = false;
    private $coremenu = array();
    private $coredefaultapp;

	public function __construct( $core ) {
        parent::__construct($core);
        $this->core = $core;
        $this->coredefaultapp = $core->config('default_app');
        //var_dump($this->core);exit;
        //var_dump($_SESSION);exit;
        //var_dump($this);exit;
    }

    protected function setup( $minimal = false ) {

        $theme = '';
        if(isset($_COOKIE['theme'])) $theme = $_COOKIE['theme'];

        $this->libmenu = new libmenu( $this->core );

        $coreui                               = new libtemplate($this->core->config('sysname'), COREVERSION, $this->core->config('apps_base_url').'/'.$this->core->config('default_app').'/assets'); // Name, version and assets folder's name
        $coreui->setCore( $this->core );
        // Global Meta Data
        $coreui->author                       = 'pixelcave';
        $coreui->robots                       = 'noindex, nofollow';
        $coreui->title                        = 'OneUI - Admin Dashboard Template & UI Framework';
        $coreui->description                  = 'OneUI - Admin Dashboard Template & UI Framework created by pixelcave and published on Themeforest';

        if($minimal) {
            $this->set_coreui($coreui);
            return;
        }

        // Global Included Files (eg useful for adding different sidebars or headers per page)
        $coreui->inc_side_overlay             = 'base_side_overlay.php';
        $coreui->inc_sidebar                  = 'base_sidebar.php';
        $coreui->inc_header                   = 'base_header.php';

        // Global Color Theme
        $coreui->theme                        = $theme;       // '' for default theme or 'amethyst', 'city', 'flat', 'modern', 'smooth'

        // Global Body Background Image
        $coreui->body_bg                      = '';       // eg 'assets/img/photos/photo10@2x.jpg' Useful for login/lockscreen pages

        // Global Header Options
        $coreui->l_header_fixed               = true;     // True: Fixed Header, False: Static Header

        // Global Sidebar Options
        $coreui->l_sidebar_position           = 'left';   // 'left': Left Sidebar and right Side Overlay, 'right;: Flipped position
        $coreui->l_sidebar_mini               = false;    // True: Mini Sidebar Mode (> 991px), False: Disable mini mode
        $coreui->l_sidebar_visible_desktop    = true;     // True: Visible Sidebar (> 991px), False: Hidden Sidebar (> 991px)
        $coreui->l_sidebar_visible_mobile     = false;    // True: Visible Sidebar (< 992px), False: Hidden Sidebar (< 992px)

        // Global Side Overlay Options
        $coreui->l_side_overlay_hoverable     = false;    // True: Side Overlay hover mode (> 991px), False: Disable hover mode
        $coreui->l_side_overlay_visible       = false;    // True: Visible Side Overlay, False: Hidden Side Overlay

        // Global Sidebar and Side Overlay Custom Scrolling
        $coreui->l_side_scroll                = true;     // True: Enable custom scrolling (> 991px), False: Disable it (native scrolling)

        // Global Active Page (it will get compared with the url of each menu link to make the link active and set up main menu accordingly)
        $coreui->main_nav_active              = basename($_SERVER['PHP_SELF']);

        // Global Main Menu
        $coreui->main_nav                     = $this->libmenu->generate_menu();
        $this->set_coreui($coreui);

    }

    private function uidata(&$data) {
        if(!isset($data['corenotifications'])) {
            $libnotification = new libnotification($this->core);
            $data['corenotifications'] = $libnotification->flush();
        }
        if(!isset($data['coreui'])) $data['coreui'] = $this->get_coreui();
        if(!isset($data['coredefaultapp'])) $data['coredefaultapp'] = $this->coredefaultapp;
        if(!isset($data['coremenu'])) $data['coremenu'] = $this->coremenu;
    }

    protected function preui($data = null, $bare = false) {
        $this->setup();
        $this->uidata($data);
        $this->view('template_head_start.php', $data, false, CORE);
        $this->view('template_head_end.php', $data, false, CORE);
        $this->view('base_head.php', $data, false, CORE);
    }

    protected function postui($data = null) {
        $this->uidata($data);
        $this->view('base_footer.php', $data, false, CORE);
        $this->view('template_footer_start.php', $data, false, CORE);
        $this->view('template_footer_end.php', $data, false, CORE);
    }

    public function show($view, $data = null, $bare = false) {

        $libnotification = new libnotification($this->core);
        $data['corenotifications'] = $libnotification->flush();
        $data['coreui'] = $this->get_coreui();
        $data['coredefaultapp'] = $this->coredefaultapp;

        if($bare == 'minimal') {
            $this->corebare = $bare;
            $this->setup( true );
            $this->view('template_head_start.php', $data, false, CORE);
            $this->view('template_head_end.php', $data, false, CORE);
            $this->view($view, $data);
            $this->view('template_footer_start.php', $data, false, CORE);
            $this->view('template_footer_end.php', $data, false, CORE);
            return;
        }

        $data['coremenu'] = $this->coremenu;

        if($bare == 'unauthorized') {
            $this->preui($data, $bare);
            $coreui = $this->get_coreui();
            $this->view($view, $data, false, CORE);
            $this->postui($data);
            exit;
        }

        if(!$bare) {
            $this->preui($data, $bare);
            $coreui = $this->get_coreui();
            $this->view($view, $data);
            $this->postui($data);
        } else
            $this->view($view, $data);
    }

	public function redirect( $destination = NULL, $app = NULL, $secure = false ) {
        if ( !( $destination != null && preg_match('/^http(s?)\:\/\//i', $destination) ) ) {
            $destination = (empty($destination)) ? $this->location() : $this->location($destination, $app, $secure); //var_dump($destination);
        }
        header( 'location: ' . trim( $destination ) );
        exit;
	}

    public function register_menu($label, $url = '', $type = 'default', $icon = null) {
        $menu = new stdClass();
        $menu->label = $label;
        $menu->url = $url;
        $menu->type = $type;
        $menu->icon = $icon;
        if($label and $url)
            $this->coremenu[] = $menu;
    }

    public function auth($authcontroller = 'auth', $authapp = null) {

        $sess = new libsession($this->core);
        $user_uid = $sess->get('user_uid');

        if($user_uid === null)
        {
            if($authapp === null) $authapp = $this->core->config('default_app');
            $this->redirect($authcontroller,$authapp);
        }

    }


}