<?php defined('CORE') or die("This file can't be accessed directly!");

class liberror {
	
	public $database = array();
	public $general = array();

	private $config;
	private $coreui;

	public function __construct( $config ) {
		$this->config = $config;
	}

	public function setup() {

		$this->coreui                               = new libtemplate('OneUI', '1.0', $this->config->config('apps_base_url').'/core/assets'); // Name, version and assets folder's name

		// Global Meta Data
		$this->coreui->author                       = 'pixelcave';
		$this->coreui->robots                       = 'noindex, nofollow';
		$this->coreui->title                        = 'Core Framework';
		$this->coreui->description                  = 'Core Framework - Multimodular web-application engine.';

		return $this->coreui;

	}

	public function show( $kind = 'all', $code = 500, $return = false, $bare = false ) {

        $errmsg = "";
		$coreui = null;

		switch($kind) {
			case 'db':
				foreach ($this->database as $e) {
                    $errmsg .= '<p class="error">'.$e.'</p>';
				}
				break;
			case 'general':
				foreach ($this->general as $e) {
                    $errmsg .= '<p class="error">'.$e.'</p>';
				}
				break;
			default:
				$errors = array_merge($this->database, $this->general);
				foreach ($errors as $e) {
                    $errmsg .= '<p class="error">'.$e.'</p>';
				}
                $this->database = array();
                $this->general = array();
				break;
		}
		if($return) return $errmsg;

        @header($_SERVER["SERVER_PROTOCOL"]." 500 Internal Server Error", true, $code);

		if(!$bare) {
			$coreui = $this->setup();
			$this->coreapp = $this->config->config('default_app');
		}

		include CORE."views/error.php";

	}

    public function add($message = NULL, $type = 'general') {
		if($type == 'db')
            $this->database[] = $message;
        else $this->general[] = $message;
    }

    public function count() {
        return count($this->database) + count($this->general);
    }

	public function location( $path = NULL, $app = NULL, $secure = false) {

		if($app == null)
			$app = preg_replace("/[^a-z0-9_-]/i","", str_replace(APPBASE, '', APPLICATION));

		$index = $this->config->config('index_file');
		$url = $this->config->config('apps_base_url') . "/"
			. ( $app != null ? $app . "/" : '' )
			. ( $index != '' ? $index . "/" : '' )
			. $path;

		if($secure)
			$url = preg_replace("/^http:/i","https:",$url); //var_dump($url);
		return $url;

	}


}
