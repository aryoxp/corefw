<?php

class libautoloader {

    private $module;
	private $app;

	public function __construct() {

        // core framework class loaders
		spl_autoload_register(array($this, 'library_loader'));
		spl_autoload_register(array($this, 'app_library_loader'));
		spl_autoload_register(array($this, 'controller_loader'));
		spl_autoload_register(array($this, 'model_loader'));

	}

	public function initcorelib()
	{
		spl_autoload_register(array($this, 'core_library_loader'));
	}

	public function autoload_appmodel( $app )
	{
		$this->app = $app;
		spl_autoload_register(array($this, 'app_model_loader'));
	}

	private function library_loader( $className ) {
		// convert the given class name to it's path
		$classPath = trim( str_replace("_", "/", $className), "/" );
		@include_once LIBRARY . $classPath . '.php';
	}

	private function core_library_loader( $className ) {
		$classPath = trim( str_replace("_", "/", $className), "/" );
		@include_once CORELIBRARY . $classPath . '.php';
	}

	private function app_library_loader( $className ) {
		// convert the given class name to it's path
		$classPath = trim( str_replace("_", "/", $className), "/" );
		@include_once APPLIBRARY . $classPath . '.php';
	}

	private function app_model_loader( $className ) {
		// convert the given class name to it's path
		$classPath = trim( str_replace("_", "/", $className), "/" );
		$modelfile = APPBASE . '/'. $this->app .'/model/'. "m" . trim( strstr( $classPath, "/" ), "/" ) .'.php';  //echo $modelfile;
		@include_once $modelfile;
	}

	private function controller_loader( $className ) {
		// convert the given class name to it's path
		$classPath = trim( str_replace("_", "/", $className), "/" );
		@include_once CONTROLLER . "c" . trim( strstr( $classPath, "/" ), "/" ) .'.php';
	}

	private function model_loader( $className ) {
		// convert the given class name to it's path
		$classPath = trim( str_replace("_", "/", $className), "/" );
		$modelfile = MODEL . "m" . trim( strstr( $classPath, "/" ), "/" ) .'.php'; //echo MODEL;
		@include_once $modelfile;
	}
}
