<?php

/**
 * Created by PhpStorm.
 * User: aryo
 * Date: 21/9/15
 * Time: 7:45 AM
 */
class basecontroller
{
    private $core;
    private $coreui;
    private $coreapp;
    private $corescripts = array();
    private $corestyles = array();
    private $coreplugins = array();

    public function __construct( $core ) {
        $this->core = $core;
        $this->coreapp = preg_replace('/[^a-z0-9-_]/i','',str_replace(APPBASE, '', APPLICATION));
    }

    protected function set_coreui($coreui) {
        $this->coreui = $coreui;
    }

    protected function get_coreui() {
        return $this->coreui;
    }

    public function view( $viewpath, $data = array(), $return = false, $base = APPLICATION ) {
        if( !$viewpath ) return;
        if( $base == APPLICATION ) $base .= 'view/';
        else if($base == CORE) $base = CORE . "views/";
        else $base .= 'view/';
        if(!isset($coreui)) $coreui = $this->coreui;
        if(is_array($data))
            extract( $data, EXTR_SKIP ); //var_dump( $data ); // extract given data arguments array as variables
        if($return) ob_start();
        if( is_readable( $base . $viewpath ) )
            include $base . $viewpath;
        else {
            $this->core->error->add( "View: <code>" . $viewpath . "</code> could not be found!" );
            if($this->core->error->count()) {
                $this->core->error->show('all', 404, false, true);
            }
        }
        if($return) {
            $content = ob_get_clean(); //var_dump($content);
            return $content;
        }
    }

    public function add_script( $scriptpath, $core = false ) {

        if(preg_match("/^http(s?):/i", $scriptpath)) {
            $this->corescripts[] = $scriptpath;
            return;
        }

        $s = null;
        if($core)
            $s = APPBASE . "/" . $this->core->config('default_app') . '/' . $this->core->config('assets_folder') . "/" . $scriptpath;
        else $s = APPLICATION . $this->core->config('assets_folder') . "/" . $scriptpath; //var_dump($s);

        if (file_exists($s)) {
            if($core)
                $script = $this->core->config('default_app');
            else $script = $this->coreapp;
            $script = $this->core->config('apps_base_url'). "/" . $script . "/" . $this->core->config('assets_folder') . "/" . $scriptpath;
            $this->corescripts[] = $script;
        }
        else if ($this->core->is_development())
            $this->core->error->add('Script not found: <code>' . $s . '</code>');

    }

    public function add_style( $stylepath, $core = false ) {

        if(preg_match("/^http(s?):/i", $stylepath)) {
            $this->corescripts[] = $stylepath;
            return;
        }

        $s = null;
        if($core)
            $s = APPBASE . "/" . $this->core->config('default_app') . '/' . $this->core->config('assets_folder') . "/" . $stylepath;
        else $s = APPLICATION . $this->core->config('assets_folder') . "/" . $stylepath; //var_dump($s);
        if(file_exists( $s )) {
            if($core)
                $style = $this->core->config('default_app');
            else $style = $this->coreapp;
            $style = $this->core->config('apps_base_url'). "/" . $style . "/" . $this->core->config('assets_folder') . "/" . $stylepath;
            $this->corestyles[] = $style; //var_dump($this->styles);
        }
        else if ($this->core->is_development())
            $this->core->error->add('Style not found: <code>' . $s . '</code>');
    }

    public function add_plugin($plugin = null) { //var_dump($plugin);

        if($plugin === null) return;
        if(in_array($plugin, $this->coreplugins)) return;

        switch($plugin) {
            case 'texteditor':
                $this->add_script('js/plugins/ckeditor/ckeditor.js', CORE);
                $this->add_script('js/init-texteditor.js', CORE);
                break;
            case 'datepicker':
                $this->add_style('css/datepicker.css', CORE);
                $this->add_script('js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js', CORE);
                $this->add_script('js/init-datepicker.js', CORE);
                break;
            case 'select2':
            case 'select':
                $this->add_style('js/plugins/select2/select2.min.css', CORE);
                $this->add_style('js/plugins/select2/select2-bootstrap.css', CORE);
                $this->add_script('js/plugins/select2/select2.full.min.js', CORE);
                $this->add_script('js/init-select.js', CORE);
                break;
            case 'fileupload':
                $this->add_style('js/plugins/fileinput/file-input.css', CORE);
                $this->add_script('js/init-fileinput.js', CORE);
                break;
            case 'validation':
                $this->add_script('js/plugins/jquery-validation/jquery.validate.min.js', CORE);
                break;
            case 'md5':
                $this->add_script('js/plugins/md5/md5.js', CORE);
                break;
            case 'map':
            case 'maps':
                $this->add_script('http://maps.google.com/maps/api/js?sensor=true');
                $this->add_script('js/plugins/gmapsjs/gmaps.min.js', CORE);
                break;
            case 'table':
            case 'datatable':
                $this->add_style('js/plugins/datatables/jquery.dataTables.min.css', CORE);
                $this->add_script('js/plugins/datatables/jquery.dataTables.min.js', CORE);
                break;
            default:
                return;
        }

        $this->coreplugins[] = $plugin;
    }

    public function location( $path = NULL, $app = NULL, $secure = false) {

        if($app == null) $app = $this->coreapp;

        $index = $this->core->config('index_file');
        $url = $this->core->config('apps_base_url') . "/"
            . ( $app != null ? $app . "/" : '' )
            . ( $index != '' ? $index . "/" : '' )
            . $path;

        if($secure)
            $url = preg_replace("/^http:/i","https:",$url); //var_dump($url);
        return $url;

    }

    public function file( $path = '', $is_core = false ) {
        if($is_core)
            $app = $this->core->config('default_app');
        else $app = $this->coreapp;
        $base = $this->core->config('apps_base_url'). "/" . $app;
        return $base . "/" . $path;
    }

    public function feat_authorized($feature_uid) {
        $user = $this->core->get_user();
        if($user === null) return false;
        if($user->uid == $this->core->config('sysadmin_uid'))
            $authorized = true;
        else $authorized = in_array($feature_uid, $user->features); //var_dump($authorized);
        return $authorized;
    }

    public function menu_authorized($menu_uid, $boolean = false) {
        $user = $this->core->get_user();
        if($user === null) {
            if($boolean) return false;
            $this->show('unauthorized.php', null, 'unauthorized');
        }
        //var_dump($this->core->config);exit;
        if($user->uid == $this->core->config('sysadmin_uid'))
            $authorized = true;
        else $authorized = in_array($menu_uid, $user->menus);
        if($authorized) {
            return true;
        } else {
            if($boolean) return false;
            $this->show('unauthorized.php', null, 'unauthorized');
        }
    }
}