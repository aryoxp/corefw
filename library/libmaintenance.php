<?php defined('CORE') or die("This file can't be accessed directly!");

class libmaintenance {

	private $config;
	private $coreui;

	public function __construct( $config ) {
		$this->config = $config;
	}

	public function setup() {

		$this->coreui                               = new libtemplate('OneUI', '1.0', $this->config->config('apps_base_url').'/core/assets'); // Name, version and assets folder's name

		// Global Meta Data
		$this->coreui->author                       = 'pixelcave';
		$this->coreui->robots                       = 'noindex, nofollow';
		$this->coreui->title                        = 'Core Framework';
		$this->coreui->description                  = 'Core Framework - Multimodular web-application engine.';

		return $this->coreui;

	}

	public function show( $bare = false ) {

		if(!$bare) {
			$coreui = $this->setup();
			$this->coreapp = $this->config->config('default_app');
		}

		include CORE."views/maintenance.php";

	}

}
