<?php

class model {

	protected $db;
	
	public function __construct( $core = null, $connection = 'default' ) {

		if($core == null)
		{
			$config = new libconfig();
			$config->load();
			$error = new liberror( $config );
			$error->add('No CORE exception in model instantiation constructor.');
			$error->show();
			exit;
		}

		$connection = $core->config->get_config()->db->$connection;
		if($connection) {
			$this->db = new libdb( $connection );
		} else {
			$core->error->add('Invalid database connection configuration.');
			$core->error->show();
		}

	}	
	
}
