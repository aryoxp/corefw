<?php defined('CORE') or die("This file can't be accessed directly!");

class libnotification {

    const all = -1;
    const info = 0;
    const warning = 1;
    const error = 2;
    const success = 3;

    private $core;

    public function __construct( $core ) {
        $core->session = new libsession( $core );
        $this->core = $core;
    }

    public function add($message, $type = self::info, $sticky = false){
        $notification = $this->core->session->get('notification');
        if(!$notification) $notification = array();
        array_push($notification, array($message,$type,$sticky));
        $this->core->session->set('notification',$notification);
    }

    public function flush($type = self::all){
        $notification = $this->core->session->get('notification');
        //var_dump($notification);
        if($type == self::all) {
            $this->core->session->remove('notification');
            return $notification;
        }

        $maintained = array();
        $flushed = array();
        foreach($notification as $n) {
            if($n[1] == $type)
                $flushed[] = $n;
            else $maintained[] = $n;
        }

        $this->core->session->set('notification',$maintained);
        return $flushed;
    }

}