<?php

class libview {

    private $user;
    private $notifications;
    private $definitions = array();
    private $menus = array();
    private $submenus;
    private $settingmenus;
    private $core;
    private $styles = array();
    private $scripts = array();

    private $module;
    private $controller;
    private $method;

    public function __construct( $core ) {
        $this->core = $core;
        $this->module = $core->module;
        $this->controller = $core->controller;
        $this->method = $core->method;
    }

    public function add_style( $stylepath ) {
        $s = APPLICATION . '/' . $this->core->config('assets_folder') . "/" . $stylepath;
        if(file_exists( $s ))
            $this->styles[]	= $stylepath;
        else echo 'Style not found: <code>'.$s.'</code>';
    }

    public function add_script($scriptpath) {
        $s = APPLICATION . '/' . $this->core->config('assets_folder') . "/" . $scriptpath;
        if(file_exists( $s ))
            $this->scripts[] = $scriptpath;
        else echo 'Script not found: <code>'.$s.'</code>';
    }

    public function get_scripts() {
        return $this->scripts;
    }

    public function get_styles() {
        return $this->styles;
    }

    public function view( $viewpath, $data = array(), $return = false, $base = MODULE ) {

        if($base == MODULE) $base .= $this->core->module . '/view/';
        else $base = VIEW;
        if(is_array($data))
            extract( $data, EXTR_SKIP ); //var_dump( $data ); // extract given data arguments array as variables
        if($return) ob_start();
        if( is_readable( $base . $viewpath ) )
            include $base . $viewpath;
        else $this->core->error->add( "View: <code>" . $viewpath . "</code> could not be found!" );
        if($this->core->error->count())
            $this->core->error->show('all', 200 );
        if($return) {
            $content = ob_get_clean(); //var_dump($content);
            return $content;
        }
    }

    public function show($view, $data = null, $bare = false) {

        $content = $this->view($view, $data, true);
        if(!$bare) {
            $data = array('sidebar'=>$this->sidebar, 'content'=>$content);
            $this->view('container.php', $data, false, CORE);
        } else echo $content;
        $this->foot($bare);

    }

    public function head($modules, $user, $bare = false) {

        $notification = new libnotification($this->core);
        $this->notifications = $notification->flush();
        $this->user = $user;
        foreach($modules as $m) {
            $mdef = MODULE . $m . '/definition.json';
            if (file_exists( $mdef )) {
                $definition = json_decode( file_get_contents($mdef) );
                $definition->module = $m;
                $this->definitions[] = $definition;
                if( property_exists($definition, 'menu'))
                    $this->menus += $definition->menu;
            }
        }

        //var_dump($this->definitions);

        $navbar['user'] = $user;   //$this->session->get("user");
        $navbar['modules'] = $modules;
        $navbar['userid'] = $user->username;
        $navbar['notifications'] = $this->notifications;

        $sidebar['definitions'] = $this->definitions;

        $sidebar['modules'] = $modules;
        $sidebar['leftmenus'] = $this->menus;
        $sidebar['settingmenus'] = $this->settingmenus;
        $sidebar['controller'] = $this->core->controller;
        $sidebar['method'] = $this->core->method;

        $this->view('header.php', null, false, CORE);
        if(!$bare) {
            $this->view('navbar.php', $navbar, false, CORE);
            $this->sidebar = $this->view('sidebar.php', $sidebar, true, CORE);
        }
    }

    public function foot($bare = false) {
        if($bare) $this->view('footer-bare.php', array('scripts'=>$this->get_scripts()), false, CORE);
        else $this->view('footer.php', array('scripts'=>$this->get_scripts()), false, CORE);
    }


    public function page_title() {
        return $this->core->config->page_title;
    }

    public function assets( $assetpath, $base = null ) {
        if($base)
            return $this->core->config->web_base_url . $this->core->config('assets_folder') . $assetpath;
        else return $this->core->config->web_base_url . 'modules/'. $this->core->module . "/" . $this->core->config('assets_folder') . $assetpath;
    }

    public function asset( $assetpath, $base = null ) {
        return $this->assets( $assetpath, $base );
    }

    public function file( $path = '' ) {
        return $this->core->config->web_base_url . $path;
    }


    /**
     * @param null, string $path
     * @param null, boolean $secure
     * @return string
     * $this->location('home', true);
     * http:// -> https://
     * $this->location('home', false);
     * https:// -> http://
     */

    public function location( $path = NULL, $secure = null ) {

        if(substr($this->core->config->index_file, strlen($this->core->config->index_file)-1, 1) != "/"
            && strlen(trim($this->core->config->index_file)) > 0 )
            $this->core->config->index_file .= "/";
        $location = str_replace( "//", "/",  $this->core->config->index_file . $path );
        $base_url = $this->core->config->web_base_url;
        if( $secure === true )
            $base_url = preg_replace('/^http\:/i', 'https:', $base_url);
        else if( $secure === false )
            $base_url = preg_replace('/^https\:/i', 'http:', $base_url);
        return  $base_url . $location;
    }

    public function location_secure( $path = "" ) {
        return $this->location( $path, true );
    }

    public function getExecutionTime() {
        return $this->core->getExecutionTime();
    }

    // Helpers

    private function module_path() {
        return MODULE . $this->core->module . "/";
    }

    private function module_view_path() {
        return $this->module_path() . "/view/";
    }

    private function module_assets_path() {
        return $this->module_path() . $this->core->config('assets_folder');
    }




}