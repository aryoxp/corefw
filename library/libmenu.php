<?php

/**
 * Created by PhpStorm.
 * User: aryo
 * Date: 5/9/15
 * Time: 8:07 PM
 */

class libmenu
{

    private $core;
    private $menu = array();
    private $user;

    public function __construct( $core ) {
        $this->core = $core;
        $this->user = $core->get_user();
    }

    private function add_dashboard() {
        //$dashboard_url = $this->core->config('apps_base_url')."/core";
        $dashboard = $this->menu('Dashboard', 'home', 'si si-speedometer');
        $dashboard['app'] = 'core';
        array_push($this->menu, $dashboard);
    }

    private function add_coremenu() {
        $app = new stdClass();
        $app->name = $this->core->config('default_app');
        $this->add_appmenu($app);
    }

    private function authorized($menuuid = null) {
        if($this->user === null) return false;
        $authorized = in_array($menuuid, $this->user->menus); //var_dump($authorized);
        return $authorized;
        //var_dump($this->user->menus); exit;
        //$menus = $this->user->menus;
    }

    private function add_appmenu( $app ) {
        //$this->authorized();
        $appmenus = array();
        $appmenudeffile = APPBASE . "/" . $app->name . "/menu.json";
        if(file_exists($appmenudeffile)) {
            $appmenu = json_decode(file_get_contents($appmenudeffile), true);
            foreach ($appmenu as $menu) {
                $menu['app'] = $app->name;
                if(isset($menu['uid'])) {
                    if($this->user->uid == '00000000-0000-0000-0000-000000000000'
                        and $app->name == $this->core->config('default_app'))
                        $authorized = true;
                    else $authorized = $this->authorized($menu['uid']);
                    if($authorized) array_push($appmenus, $menu);
                } else {
                    array_push($appmenus, $menu);
                }
            }
        }
        $headingonly = true;
        foreach($appmenus as $m) {
            if(!isset($m['type'])) {
                $headingonly = false;
                break;
            }
        }
        if(!$headingonly) {
            foreach ($appmenus as $m) {
                array_push($this->menu, $m);
            }
        }

        $menu = $this->menu[count($this->menu)-1];
        if(isset($menu['type']) and $menu['type'] == 'heading') {
            array_pop($this->menu);
        }

    }

    private function heading($name) {
        return $this->menu($name, null, null, null, 'heading');
    }

    private function menu($name, $url = '#', $icon = null, $sub = null, $type = null) {
        $menu = array(
            'name' => $name,
            'url' => $url,
            'icon' => $icon,
            'sub' => $sub,
            'type' => $type
        );

        if($icon == null) unset($menu['icon']);
        if($sub == null) unset($menu['sub']);
        if($type == null) unset($menu['type']);
        return $menu;
    }

    public function generate_menu(){

        $this->add_dashboard();

        $apps = $this->core->config->apps();
        foreach($apps as $app)
            $this->add_appmenu($app);

        $this->add_coremenu();

        foreach($this->menu as &$menu) { //var_dump($menu);
            $menu['name'] = '<span class="sidebar-mini-hide">'.$menu['name'].'</span>';
        }

        $jsonmenu = json_encode($this->menu);
        return json_decode($jsonmenu, true);
    }
}