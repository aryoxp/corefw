<?php defined('CORE') or die("This file can't be accessed directly!");

class module extends basecontroller {

    private $core;
    private $app;

	public function __construct( $core ) {
        parent::__construct($core);
        $this->core = $core;
        //$backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $module = $backtrace[1]['class'];
        $this->app = str_replace("module_", '', $module);
        $this->core->autoload_appmodel( $this->app );
        $this->core->config->load_appconfig( $this->app );
        //var_dump($this->core->config);
    }

    public function view($viewpath, $data = null, $return = false, $base = APPLICATION) {
        $base = APPBASE."/".$this->get_app()."/";
        parent::view($viewpath, $data, false, $base);
    }

    public function get_app() {
        return $this->app;
    }

    public function authorized( $role_uid = null) {
        $user = $this->core->get_user(); //var_dump($user);

        if($role_uid === null) {
            $appdeffile = APPBASE . '/' . $this->app . "/defs.json";
            if(file_exists($appdeffile) and $json = file_get_contents($appdeffile)) {
                $appconfig = json_decode($json);
                if (property_exists($appconfig, 'roles')) {
                    foreach ($appconfig->roles as $role) {
                        foreach ($user->roles as $urole) {
                            if ($urole->role_uid == $role->uid) return true;
                        }
                    }
                }
            }
        } else if(is_array($role_uid)) {
            foreach ($role_uid as $role) {
                foreach ( $user->roles as $urole ) {
                    if($urole->role_uid == $role) return true;
                }
            }
        } else {
            foreach ( $user->roles as $urole ) {
                if($urole->role_uid == $role_uid) return true;
            }
        }
        return false;
    }

}