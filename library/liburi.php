<?php defined('CORE') or die("This file can't be accessed directly!");

class liburi {

	private $uristring;
	private $fulluristring;
	private $controller;
	private $method;
	private $args;

    public function __construct( $config ){

		$this->config		    = $config;
		$this->fulluristring 	= $_SERVER['REQUEST_URI']; //var_dump($_SERVER['REQUEST_URI']); echo "<br>";

		$path = NULL;

		if(isset( $_SERVER['PATH_INFO'] )) $path = $_SERVER['PATH_INFO'];
		else {

			$prepath = str_replace("/index.php", "", $_SERVER['SCRIPT_NAME']);
			$path = str_replace($prepath, "", $_SERVER['REQUEST_URI']);
            if($path == '/'.$this->config->index_file)
                $path = "";
		} 
		$path = trim( $path, "/" ); // removes trailing and leading slashes if any
		$this->uristring = $path;
		$this->args = array(); // as arguments array storage

        $segments = explode( "/", $this->uristring ); //echo "<pre>"; var_dump($path); var_dump($segments); exit;

		if( count( $segments ) && trim($segments[0]) != "" ) {

            $segments = array_splice($segments,0,count($segments));
			//var_dump($segments);

            if( count( $segments ) ) {

                while (count($segments) > 0) {

                    $f = APPLICATION .'controller/c'. implode('/', $segments) . ".php";

                    if ( file_exists($f) ) {

                        $this->controller = implode("/", $segments);

                        if( !$this->method = array_pop( $this->args ) )
                            $this->method = $this->config->default_method;
                        break;

                    } else
						$this->args[] = array_pop($segments);

                }
            } else {

                $this->controller = $this->config->default_controller;
                $this->method = $this->config->default_method;
                $this->args = array();
            }

		} else {

            $this->controller = $this->config->default_controller;
            $this->method = $this->config->default_method;
            $this->args = array();
        }

        $this->args = array_reverse($this->args);
		if($this->controller)
			$this->controller = "controller_" . str_replace( "/", "_",  $this->controller );
		else $this->controller = NULL;

    }

    public function getUriString(){
		return $this->uristring;
    }
	
	public function getFullUriString(){
		return $this->fulluristring;
	}
	
	public function getController() {
		if( isset($this->config->maintenance) and $this->config->maintenance ) {
            $this->controller = 'controller_maintenance';
        } else if (empty($this->controller)) {
			$this->controller = NULL;
		} else if( isset( $this->config->filter_regex ) ) {
            $this->controller = preg_replace($this->config->filter_regex, '', $this->controller);
        }
		return strtolower($this->controller);
	}
	
	public function getMethod() {

		if( isset( $this->config->filter_regex ) )
			$this->method = preg_replace( $this->config->filter_regex, '', $this->method );

		if( method_exists($this->controller, $this->method) ) {
			return strtolower($this->method);
		} else {
            return strtolower($this->method);
        }

	}

	public function getArgs() {
		return $this->args;
	}

} 

// end of library URI
