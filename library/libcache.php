<?php defined('CORE') or die("This file can't be accessed directly!");

class libcache {

	private $core;

	public function __construct( $core ) {
		$this->core = $core;
	}

	public function cache($data, $file = 'cache.serial', $format = 'serial') {

		$cachefile = CACHE . $file;
		if(!file_exists($cachefile)) {
			$paths = explode("/", $file);
			$file = array_pop($paths);
			$path = implode("/", $paths);
			if($path)
				mkdir($path, 0777, true);
		}

		touch($cachefile);

		if(is_writable($cachefile)) {

			switch($format) {
				case 'json':
					$data = helper::utf8ize($data);
					file_put_contents($cachefile, json_encode($data));
					break;
				default :
					file_put_contents($cachefile, serialize($data));
					break;
			}

		} else {
			$this->core->error->add('Cache file does not exists nor writable.');
			$this->core->error->show();
			exit;
		}
	}

	public function read($file = 'cache.serial', $expired = 3600, $format = 'serial') {
		$data = null;
		$cachefile = CACHE . $file;

		if(is_readable($cachefile)) {
			$mtime = filemtime($cachefile);
			if(time() - $mtime > $expired) return null;
			//var_dump(time() - $mtime);
			switch($format) {
				case 'json':
					$data = json_decode(file_get_contents($cachefile));
					break;
				default:
					$data = unserialize(file_get_contents($cachefile));
					break;
			}

		}

		return $data;

	}

}
