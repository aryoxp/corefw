<?php defined('CORE') or die("This file can't be accessed directly!");

class libconfig {

    private $config;
    private $appscachefile = 'apps.json';

    const CORE_CONFIG_FILE = 'config/core.json';

    public function load() {

        if(file_exists(CORE . self::CORE_CONFIG_FILE))
            $this->config = json_decode(file_get_contents(CORE . self::CORE_CONFIG_FILE));
        //var_dump($this->config);
        if(file_exists(APPLICATION . 'config.json'))
            $this->config->config = (object)array_merge((array)$this->config->config,
                (array)json_decode(file_get_contents(APPLICATION . 'config.json')));
        //var_dump($this->config);

        switch ($this->config('environment')) {
            case 'production':
                error_reporting(0); break;
            case 'dev':
            case 'development':
                ini_set('display_errors',1);
                error_reporting(E_ALL); break;
            default:
                $error = new liberror( $this );
                $error->add('The application environment configuration is not set correctly.<br>
				    Please check your <code>environment</code> key configuration in <code>config.json</code> file.<br>
				    Valid value is <code>production</code>, <code>dev</code>, or <code>development</code>');
                $error->show();
                exit;
        }
    }

    public function load_appconfig( $app ) {
        $configfile = APPBASE . '/' . $app . '/' . 'config.json'; //var_dump($configfile);
        if(file_exists($configfile))
            $this->config->config = (object)array_merge((array)$this->config->config,
                (array)json_decode(file_get_contents($configfile)));
    }

    public function apps() {
        $apps = array();
        $appscache = APPBASE.'/'.$this->config('default_app').'/config/' .  $this->appscachefile;
        if(file_exists($appscache)) {
            $apps = json_decode(file_get_contents($appscache));
        }
        return $apps;
    }

    public function config($key, $configkey = 'config') {
        return $this->config->$configkey->$key;
    }

    public function get_config($configkey = 'config') {
        return $this->config->$configkey;
    }

}