<?php

$GLOBALS['microtime_start'] = microtime(true);

if(!APPLICATION)
	die("Unable to find APPLICATION directory definition in application configuration file, the system will now exit.");

define('CORE', dirname(__FILE__) . "/");
define('COREVERSION', "v1.1");
require_once CORE . "library/libcore.php";
$core = new libcore();
$core->start();
$core->cleanup();
exit;